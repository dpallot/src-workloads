
- [SRC Scientific Workflows](#src-scientific-use-cases)
- [Directory structure](#directory-structure)
- [Adding a new applications or workflow](#adding-a-new-applications-or-workflow)

# SRC Scientific Workloads

## Introduction 

The goal of this repository is to collect runnable scientific **workloads** that will be expected to run at SKA Regional Centers (SRC). We classify workloads into **tasks** and **workflows**. A task is a small piece of code that achieves a step towards a science result, this could be source-finding, mosaicking, image cutting, etc. A workflow is a collection of these tasks that achieve a more detailed scientific result, and we are investigating using a workflow language to achieve this.

Each of the tasks and workflows should contain all the necessary information to be reproduced. This means any software required should be in a container with the Docker file described in the repository. The steps to build and run the Docker file should be stated, along with any commands the user should type inside the container. For example, bash or python commands should be described. Equally, required input data should be appropriately referenced along with download instructions. You should put yourself in the shoes of someone who has never run your code before, and describe the steps so they can copy and paste commands as required, or run pre-defined scripts. 

### Workload mandatory requirements

Each workload should meet a set of minimal requirements. The purpose of enforcing these requirements is to ensure that all workloads are organised and structured following the same rules, and that all the required information that is necessary for running the workload is included.

Therefore, each workload should meet at least the following requirements:
  - **Input data should be referenced** with links and download instructions, and not added to the repository unless they are very small (<1MB).
  - A **Dockerfile** or Singularity container definition file along with any dependencies needed to build the task as a container.
  - A `README.md` file containing at least:
	- A general description of the workload and purpose.
  	- **Detailed build instructions** for building the container, stating versions and architecture for any dependencies.
  	- **Detailed running instructions** for the workload using the default data, detailing or referencing all run parameters.
	- A **description of the expected outputs**:
	  - Approximate runtime or performance results.
	  - Do not include output data products, but do include screenshots or values that show the expected result for a successful run.
	- **Hardware dependencies**. This involves specifying whether the code supports accelerators (such as GPUs), and whether these accelerators are required or optional. State any other hardware dependencies (e.g. if the code is only compatible with a specific network fabric, detail this).
  - A script in `scripts/get-data.sh` so that downloading the example data can be separated from the execution of the task. In case the data
  - A script in `scripts/run.sh` that executes the task with the example data. This is so we can automate running all tasks for system testing, performance regression testing, and benchmarking.
  - A script in `scripts/build.sh` which builds the workload/task container image. This will be used both by users and the CI system.
  - Tasks should be built automatically into a corresponding container image using the CI system. (You can expect assistance by the repository maintainers to setup this integration.)
  - Adding a new task means you are happy to collaborate on maintaining it and discussing updates via merge requests. 

### Workload optional data

  In addition, we recommend adding the following optional information to each workload. These are especially relevant for tasks:

  - *In the README.md*: How to use the task with alternative data, e.g. editing relevant parameter files and highlighting any potential problems with unique data sets.
  - *Linked in the README.md*: A recorded demo of how to run the task.
  - *In a `contrib/` directory*: Include any relevant runtime parameters such as CPU affinity settings, recommended memory/core ratio, and performance scalability properties of the workload (strong scaling and/or weak scaling). You're welcome to include any data that helps understand the workload behaviour and aid in running it efficiently, such as batch submission scripts and/or graphs.
  - *In a `contrib/` directory*: Power and energy efficiency data (aggregated or time series).
  - *In a `contrib/` directory*: Profiling data, and accompanying performance analysis results or conclusions.
  - Automatic running of the workload in a remote site via CI pipelines.

## Directory structure

In order to be able to structure and scale all of this information, this repository is organised in the following hierarchical structure:

```
├── README.md
├── Tasks  # Tasks are independent steps towards a scientific result.
    ├── Mosaicking-SWARP
        ├── Dockerfile
	├── README.md
	├── contrib
	└── scripts
		├── build.sh
		├── get-data.sh
		├── lofar
		│   ├── get-lofar-data-parallel.sh
		│   ├── lofar-files.csv
		│   └── plot-mosaic.ipynb
		├── run.sh
		└── sdss
		├── J011700.00+280000.0.small.sh
		└── get-sdss-data-parallel.sh
│   ├── Image-coadding-SWARP
│   ├── Source-finding
│   ├── Image-cutouts
│   
└── workflows            # Workflows are a collection of independent tasks, with scripts that wrap them together so a user can obtain a complex science result.
    │                   
    ├── HI-FRIENDS
    ├── eMerlin
```

We expect to organise tasks further into categories. It is unclear how broad these categories will be. We plan to decide this once we gather a significant amount of tasks, and it becomes more apparent how to scale these into categories.

Workflows consists of one or more tasks, and each task runs one application with certain inputs. Each task can be run multiple times, resulting in individual runs where certain parameters (input stays the same) may change, such as number of nodes/processes, the SRC node, or on the computation device. Workflows may be maintained as separate git repositories. Ideally, workflows reference any tasks used from this repository, but could include more unique aspects inside the workflow repository. For example, if a workflow does mosaicking, source-finding and source classification, each of those steps would be separate tasks, and they would be brought together using a workflow description language that references the tasks and provides all the information required to stitch them together into a complete workflow run. A workflow may include tasks that are not yet integrated into this repository, and so this should be stated clearly in the workflow with a note on if it makes sense to include this as a task in the future. 

Note: The exact structure for workflows is yet to be determined once we have a fully working example.

## Adding a new Task or Workflow

Adding a new Task or Workflow should replicate the example structure detailed above. We want all Tasks to be directly hosted in this repo, but exceptions can be made where it is more appropriate to add them as sub-modules that mirror another repo. Large workflows may already be hosted in other Git repositories, and so adding them as sub-modules may be preferred. Note however that in the future we plan to use CI/CD to build containers automatically and batch test tasks, which requires the task to be directly hosted in this repo. If you are on the SKAO Gitlab account you will be a "developer" who can create feature branches and do merge requests. A "maintainer" will then approve the merge request. 

To add a new Task, you should do the following from the command line:

Create a new feature branch and then submit a merge request following these instructions: https://docs.gitlab.com/ee/gitlab-basics/feature_branch_workflow.html

At stage 4 of those instructions, if you want to add it as a sub-module do the following:

Move to the task directory:

`cd src-workloads/tasks`

Add the Git repo for your task as a sub module, where the path is the http url, and give it an appropriate folder name:

`git submodule add <path-to-your-task-repo> <folder-name-for-task>`

Then continue with steps 5/6/7/8/9 in the above instructions to push the changes to the branch you created and then submit a merge request to the main branch. A maintainer will then approve your merge request and the branch will be merged into the main. 
