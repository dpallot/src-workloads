# Source-finding task using PyBDSF

This task will run the source-finder PyBDSF on a LOFAR image from the LoTSS DR2 survey.

## How to use the container

Clone this repository

Build the image

    docker build . -f ./Dockerfile --tag pybdsf:latest

Run the container, mounting your path so we can access data inside or outside of the container

    docker run --name pybdsf -d -t -v "$(pwd)"/scripts:/opt/scripts/ pybdsf:latest

Exec into the container where you can run swarp from the command line

    docker exec -it pybdsf /bin/bash

You should then run the run-all.sh script which will download the data and run the source-finding.py script.

## How to use a Jupyter notebook

SWARP is run from the command line, but you can use a Jupyter notebook to run this using the terminal, and then plot the images using Python.

To use a Jupyter notebook environment, change to the Jupyter directory

    cd Jupyter

then build the container

    docker build . -f ./Dockerfile-jhub --tag pybdsfjupyter:latest

then run the container and a link will appear which you can copy into your browser to access

    docker run -p 8888:8888 -v "$(pwd)"/scripts:/home/jovyan/scripts --user root -e GRANT_SUDO=yes pybdsfjupyter

You should then start a terminal to run the run-all.sh script which will download the data and run the source-finding.py script.

Finally, you can run carta to interactively explore the plot and source catalogue

    docker run -ti -p 3002:3002 -v $PWD:/images cartavis/carta:latest