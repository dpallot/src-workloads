from setuptools import setup

setup(
    name='cutouts',
    version='0.2.0',
    py_modules=['cutouts','check_LoTSS','get_LoTSS_images','sort_json'],
    install_requires=[
        'Click',
    ],
    entry_points={
        'console_scripts': [
            'cutouts = cutouts:cli',
            'sort_json = sort_json:cli',
            'check_LoTSS = check_LoTSS:cli',
            'get_LoTSS_images = get_LoTSS_images:cli',
        ],
    },
)